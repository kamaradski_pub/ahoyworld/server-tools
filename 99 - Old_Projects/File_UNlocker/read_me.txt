// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// File:			unlock.bat
// Author:			Kamaradski 2014
// Contributers:		none
//
// Arma3 file unlocker script written for Ahoyworld.co.uk
//
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-


What:
This script loops through all files with a sepcific extension, in the folder where it is located, checks if the file is locked, and unlocks it for normal use


How:
1 - Edit the file extension the script is running for (default: *.PBO)
2 - Edit the Path to where handle.exe is located (Default: C:\apps\handle)
3 - Copy the script in to the directory where you want it to run
4 - Manually run the script by double-clicking it, or start it from another script (example: restarter script)


Dependancies:
This script requires handle.exe from the Windows Sysinternals toolset.
http://technet.microsoft.com/en-us/sysinternals/bb896655.aspx


The user can:
- Configure the path to handle.exe
- Configure what file-extension the script will run against


What does Ahoyworld use it for ?
- We call this script 10 secends after the A3 server is started (or restarted)
- It unlocks all MPMissions PBO's
- This allows us to sync the MPMissions folders between the servers, and sync it to our local PC's with btsync for easy updating
- This also allows us to delete MPMissions via btsync from the local PC, without the need to restart the complete server


Contribution:
In case you like to improve this script: Please FORK it on Bitbucket Git, and send us a pull-request