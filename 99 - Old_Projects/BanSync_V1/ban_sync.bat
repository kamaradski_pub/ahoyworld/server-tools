@echo off

REM =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
REM ARMA3 remote banlist sync script by Kamaradski for AhoyWorld.co.uk
REM =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-


REM =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
REM 						Script variables
REM =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

REM Set BanSync history directory path:
set hisdir=C:\Program Files (x86)\Steam\SteamApps\common\Arma 3\bansync\History

REM Set logfile directory path:
set logdir=C:\Program Files (x86)\Steam\SteamApps\common\Arma 3\bansync\Logs

REM Set FTP-script location path:
set ftpscr=C:\Program Files (x86)\Steam\SteamApps\common\Arma 3\bansync\FTPscripts

REM Set Whitelist and unban location:
set unban=C:\Program Files (x86)\Steam\SteamApps\common\Arma 3\bansync\Unban

REM Set ARMA3 path, for local ban.txt:
set a3dir=C:\Program Files (x86)\Steam\SteamApps\common\Arma 3

REM Set interval time:
set runinterval=570

REM Set remote address:
set remadd=216.231.138.138

REM set compare values:
set compset=Fvxf

REM =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
REM 						Script Engine
REM =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

choice /T 15 /D Y /M "Do you want to clear the log-files ? (Yes/No)"
IF ERRORLEVEL ==2 goto start

del "%logdir%\*.*"

:start
echo. >> "%logdir%\commandout.txt"
echo ********** starting bansync %DATE% %TIME% >> "%logdir%\commandout.txt"

echo Downloading Server1 banlist from Viper...
echo. >> "%logdir%\FTP-bansync.log"
echo ********** Download started %DATE% %TIME% ********** >> "%logdir%\FTP-bansync.log"
ftps -e:on -s:"%ftpscr%\dl_s1.scr" %remadd% -z >> "%logdir%\FTP-bansync.log" 2>&1
rename ban.txt ban1.txt >> "%logdir%\commandout.txt" 2>&1

echo Downloading Server2 banlist from Viper...
echo. >> "%logdir%\FTP-bansync.log"
echo ********** Download started %DATE% %TIME% ********** >> "%logdir%\FTP-bansync.log"
ftps -e:on -s:"%ftpscr%\dl_s2.scr" %remadd% -z >> "%logdir%\FTP-bansync.log" 2>&1
rename ban.txt ban2.txt >> "%logdir%\commandout.txt 2>&1"

echo Preparing EU-banlist for sync...
copy "%a3dir%\ban.txt" banEU.txt >> "%logdir%\commandout.txt" 2>&1

echo Merging ban1.txt and ban2.txt
grep -%compset% ban1.txt ban2.txt | sort >> ban1.txt 

echo Merging banEU.txt and ban1.txt
grep -%compset% banEU.txt ban1.txt | sort >> ban1.txt

echo Checking for UID's to unban...
grep -v --file="%unban%\unban.txt" ban1.txt | sort > masterban1.txt

echo Unbanning whitelist...
grep -v --file="%unban%\adminwhitelist.txt" masterban1.txt | sort > masterban2.txt

echo Updating Caroline and Historic log...
copy masterban2.txt "%a3dir%\ban.txt" >> "%logdir%\commandout.txt" 2>&1
copy masterban2.txt ban.txt >> "%logdir%\commandout.txt" 2>&1
move masterban2.txt "%hisdir%\ban-%time:~0,2%%time:~3,2%-%DATE:/=%.txt" >> "%logdir%\commandout.txt" 2>&1
move masterban2.txt "%hisdir%\ban-%time:~1,1%%time:~3,2%-%DATE:/=%.txt" >> "%logdir%\commandout.txt" 2>&1

echo Upload banlist to Server1...
echo. >> "%logdir%\FTP-bansync.log"
echo ********** Upload started %DATE% %TIME% ********** >> "%logdir%\FTP-bansync.log"
ftps -e:on -s:"%ftpscr%\ul_s1.scr" %remadd% -z >> "%logdir%\FTP-bansync.log" 2>&1

echo Upload banlist to Server2...
echo. >> "%logdir%\FTP-bansync.log"
echo ********** Upload started %DATE% %TIME% ********** >> "%logdir%\FTP-bansync.log"
ftps -e:on -s:"%ftpscr%\ul_s2.scr" %remadd% -z >> "%logdir%\FTP-bansync.log" 2>&1

echo Waiting before cleanup:
timeout /T 30
echo Cleaning files and purging History...
del *.txt >> "%logdir%\commandout.txt" 2>&1
forfiles -p "%hisdir%" -s -m *.txt /D -2 /C "cmd /c del @path"  >> "%logdir%\commandout.txt" 2>&1

echo Time before re-sync:
timeout /T %runinterval%
goto start