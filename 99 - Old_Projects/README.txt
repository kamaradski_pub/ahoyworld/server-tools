Collection of undocumented old projects that are not in use anymore, or never finalized.

It might provide someone with some inspiration for a working product.

Do not expect any of this to work out of the box (or at all), no warranty on these at all.