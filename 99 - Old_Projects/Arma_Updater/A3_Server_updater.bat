@echo off
SETLOCAL ENABLEDELAYEDEXPANSION

REM -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
REM File:			A3_Server_updater.bat
REM Version:		V1.4
REM Author:			Kamaradski 2014
REM Contributers:		Unknown
REM
REM Arma3 Server updater script written for Ahoyworld.co.uk
REM
REM -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-


REM =-=-=-=-=-=-=-=-=-=-=-=-=-=- CONFIGURATION -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
REM STEAMPATH = Your path SteamCMD
REM A3MasterPath = Location where this script can store a permanent master-copy of the Arma3 server
REM StartList = The root folder where your live Arma server(s) are running
REM LogLoc = Location for the logfile

SET "STEAMPATH=C:\Games\Tools\SteamCMD"
SET "A3MasterPath=C:\Games\Tools\A3_Master"
SET "StartList=C:\Games"
Set "LogLoc=C:\Games\Tools\A3_Scripts\log"

REM =-=-=-=-=-=-=-=-=-=-=-=-=- DO NOT EDIT BELOW HERE -=-=-=-=-=-=-=-=-=-=-=-=-=-

cls
echo.
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo                AhoyWorld.co.uk Arma3 updater by Kamaradski
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo.
echo ALL SERVERS INSTANCE(S) YOU ARE ABOUT TO UPDATE SHOULD BE TURED OFF !!!
echo.
SET /P "_opt=ARE YOU SURE YOU WANT TO CONTINUE (Y/N)?"
if /i %_opt% EQU y goto :confirmed
if /i %_opt% EQU yes goto :confirmed
goto elsthenyes

:elsthenyes
echo You have chosen: %_opt%
echo Execution of this script will now terminate...
PING -n 3 127.0.0.1 > nul
exit

:confirmed
set _opt=""
set looppass=0
cls
echo.
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo                AhoyWorld.co.uk Arma3 updater by Kamaradski
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo.
echo Enter your Steam account details below:
echo It will ask for the validation code if needed.
echo.
SET /P "STEAMLOGIN=Steam login name:"
SET /P "STEAMPASS=Steam password:"
echo.
SET /P "_opt=To what version do you want to update, or Quit? (DEV/STABLE/Q)?"
if /i %_opt% EQU DEV SET A3BRANCH=107410 -beta development & goto continuehere
if /i %_opt% EQU STABLE SET A3BRANCH=107410 -beta & goto continuehere
if /i %_opt% EQU Q goto elsthenyes
echo .
echo The option you have chosen is not valid. Type DEV or STABLE
PING -n 3 127.0.0.1 > nul
goto confirmed
:continuehere
echo.
echo updating ArmA3 to local update cache ...
echo     Local update cache: %A3MasterPath%
echo     Branch: %A3BRANCH%
echo.
PING -n 3 127.0.0.1 > nul
%STEAMPATH%\steamcmd.exe +login !STEAMLOGIN! !STEAMPASS! +force_install_dir %A3MasterPath% +"app_update %A3BRANCH%" validate +quit
PING -n 11 127.0.0.1 > nul
:jumpback
cls
set looppass=0
echo.
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo                AhoyWorld.co.uk Arma3 updater by Kamaradski
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo.
echo Select the Arma_server you want to update:
echo.
For /d %%i IN ("%StartList%\*") DO (
	if "%%i" NEQ "%A3MasterPath%" (
		if exist "%%i\arma3server.exe" (
			set /a looppass+=1
			echo !looppass! - %%~ni
		)
	)
)
goto proceed
exit

:proceed
echo.
echo A - Update ALL server instance(s) listed
echo Q - EXIT
echo.
Set /P "_opt=Please type selection number:"
If /i "%_opt%" EQU "A" goto updateall
If /i "%_opt%" EQU "Q" goto elsthenyes
goto updateone

:updateall
echo.
echo Updating all listed server instance(s), please be patient:
For /d %%i IN ("%StartList%\*") DO (
	if "%%i" NEQ "%A3MasterPath%" (
		if exist "%%i\arma3server.exe" (
			echo Processing: %%~ni
			PING -n 3 127.0.0.1 > nul
				robocopy "%A3MasterPath%" "%%i" /mir /XD logs Keys MPMissions BattlEye TADST userconfig /XF *.bat *.txt *.cfg *.log *.rpt *.config *.zip TADST.exe /LOG:"%LogLoc%\update.log" /tee /np
				robocopy "%A3MasterPath%\BattlEye" "%%i\BattlEye" /mir /XD BattlEye /XF *.bidmp *.bat *.txt *.log *.rpt *.zip /LOG+:"%LogLoc%\update.log" /tee /np
			echo.
		)
	)
)
goto updateready

:updateone
set looppass=0
echo.
echo Updating selected server instance, please be patient:
echo %_opt%
For /d %%i IN ("%StartList%\*") DO (
	if "%%i" NEQ "%A3MasterPath%" (
		if exist "%%i\arma3server.exe" (
			set /a looppass+=1
			if !looppass! EQU %_opt% (
				echo Processing: %%~ni
				PING -n 3 127.0.0.1 > nul
				robocopy "%A3MasterPath%" "%%i" /mir /XD logs Keys MPMissions BattlEye TADST userconfig /XF *.bat *.txt *.cfg *.log *.rpt *.config *.zip TADST.exe /LOG:"%LogLoc%\update.log" /tee /np
				robocopy "%A3MasterPath%\BattlEye" "%%i\BattlEye" /mir /XD BattlEye /XF *.bat *.txt *.bidmp *.log *.rpt *.zip /LOG+:"%LogLoc%\update.log" /tee /np
				echo.
				goto jumpback
			)
		)
	)
)
echo Your selection does not exist: returning to menu in 5 seconds.
PING -n 6 127.0.0.1 > nul
goto jumpback

:updateready
cls
echo.
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo                AhoyWorld.co.uk Arma3 updater by Kamaradski
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo.
echo.
echo Your ArmA3 is now up to date and you can re-start your server instance(s)
echo.
echo This window will automatically close after 10 seconds.
echo.
PING -n 11 127.0.0.1 > nul

exit